package bibleReader.model;

// TODO Add Javadoc comments throughout the class.

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Gunnar Anderson (provided the implementation)
 */
public class Reference implements Comparable<Reference> {
	/*
	 * Add the appropriate fields and implement the methods. Notice that there are
	 * no setters. This is intentional since a reference shouldn't change.
	 * Important: You should NOT store the book as a string. There are several
	 * reasons for this that will be clear if you think about it for a few minutes.
	 * There is already a class that you can use instead.
	 */
	/**
	 * here our the fields I used. I stored book as a BookOfBible, not a string
	 */
	private BookOfBible book;
	private int chapter;
	private int verse;

	/**
	 * 
	 * @param book
	 * @param chapter
	 * @param verse
	 */
	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;

	}

	/**
	 * 
	 * @returns string representation of a BookOfBible
	 */

	public String getBook() {
		return book.toString();
	}

	/**
	 * returns BookOfBible of the reference
	 * 
	 */

	public BookOfBible getBookOfBible() {
		return book;
	}

	/**
	 * 
	 * @return chapter
	 */

	public int getChapter() {
		return chapter;
	}

	/**
	 * 
	 * @return verse
	 */

	public int getVerse() {
		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		String format;
		format = this.getBook() + " " + this.getChapter() + ":" + this.getVerse();
		return format;
	}

	/**
	 * equals method that checks if the Object is a reference, and if it is casts it
	 * as such Uses String representation of the book to check equality checks
	 * equality with the integers "chapter" and "verse" Checks hashcode with
	 * overidden hashcode method
	 */

	@Override
	public boolean equals(Object other) {
		if (other instanceof Reference) {
			Reference ref = (Reference) other;
			if (this.getBook().equals(ref.getBook())) {
				if (this.getChapter() == ref.getChapter() && this.getVerse() == ref.getVerse()) {
					if (this.hashCode() == ref.hashCode())
						return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		// I'll give you this one as a freebie. This is a trick I learned from
		// Dr. McFall.
		// If two instances are the same, their toString method will return the
		// same thing (assuming you implement toString properly).
		// Thus calling hashCode on them will return the same int, so we are
		// honoring the contract that if a.equals(b) then
		// a.hashCode==b.hashCode.
		// Why go to extra trouble to implement hashCode based on the fields and
		// some weird formula when String already has an implementation?
		// (Well, maybe because it is perhaps less efficient, but we'll do it
		// anyway since it probably isn't that bad.)
		return toString().hashCode();
	}

	@Override
	public int compareTo(Reference otherRef) {
		// TODO Implement me: Stage 2
		// See the Javadoc for the Comparable interface for what this method is
		// supposed to do. Read the documentation for compareTo very carefully.
		//
		// Hint: Enums (like BookOfBible) implement the Comparable interface.
		// To compare int types, there are at least two good choices. One
		// involves using the Integer class (which implements the Comparable
		// interface). The other involves simple arithmetic and is simpler and
		// more efficient.
		return 0;
	}
}
