package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Gunnar Anderson (provided the implementation)
 */
public class Verse implements Comparable<Verse> {
	/**
	 * field of type Reference and a String that is the verse's text
	 */
	private Reference reference;
	private String verseT;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r The reference for the verse
	 * @param t The text of the verse
	 */
	public Verse(Reference r, String t) {
		reference = r;
		verseT = t;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 * @param text    The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		reference = new Reference(book, chapter, verse);
		verseT = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		return reference;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		return verseT;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 */
	@Override
	public String toString() {
		String format;
		format = reference.toString() + " " + verseT;
		return format;
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		if (other instanceof Verse) {
			Verse v = (Verse) other;
			if (this.getReference().equals(v.getReference())) {
				if (this.getText().equals(v.getText())) {
					if (this.hashCode() == v.hashCode()) {
						return true;
					}
				}
			}
		}
		return false;

	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Verse other) {
		// TODO Implement me: Stage 2
		return 0;
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
