package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;


/*
 * Tests for the Verse class.
 * @author's Quentin Couvelaire and Gunnar Anderson
 */

public class Stage01StudentVerseTest {
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	private Verse v1;
	private Verse v2;
	private Verse v3;
	private Verse v4;
	@Before
	public void setUp() throws Exception {
		// Create some verse objects using the two different verse constructors
		
		v1 = new Verse(new Reference(BookOfBible.Ruth, 1, 1), "Book called Ruth");
		v2 = new Verse(new Reference(BookOfBible.Genesis, 1, 1), "Book called Ruth");
		v3 = new Verse(BookOfBible.Ruth, 1, 1, "A book called Ruth");
		v4 = new Verse(BookOfBible.Revelation, 1, 1, "A book called Ruth");
		
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	
	@Test
	public void testgetReference() {
		assertEquals(new Reference(BookOfBible.Ruth, 1, 1), v1.getReference());
		assertEquals(new Reference(BookOfBible.Genesis, 1, 1), v2.getReference());
	}
	
	@Test
	public void testgetText() {
		assertEquals("Book called Ruth", v1.getText());
		assertEquals("A book called Ruth", v3.getText());
	}
	
	@Test
	public void testtoString() {
		Verse id = v4;
		assertEquals(id.toString(), v4.toString());
	}
	
	@Test
	public void testEquals() {
		Verse id = v4;
		assertTrue(id.equals(v4));
		assertFalse(id.equals(v1));
	}
	
	@Test
	public void testhashCode() {
		Verse id = v4;
		// should have same hashcode, as they are the same object
		assertEquals(id.hashCode(), v4.hashCode());
	}
	
	@Test
	public void testcompareTo() {
		
	}
	
	@Test
	public void testsameReference() {
		Verse id = v1;
		assertTrue(id.sameReference(v1));
		assertFalse(id.sameReference(v2));
	}
	
}
